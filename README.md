# Micpneu

MicPneu est une marketplace de ventes de pneus à destination de professionnels. Ce projet sera une application web. Nous sommes l’équipe UI et nous allons-nous charger de mettre en place le design système du projet. 

## Getting started

Installation du projet :
```
npm install
````

Lancer le projet :
```
npm start
```

## Folder structure

- src
  - [UI](./src/ui/readme.md)

## Liscense

MIT
